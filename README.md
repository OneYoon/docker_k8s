# Docker

## Docker 란?


준비물!!


### Docker 설치

Docker Site의 Link는 넣지만 Version이 빠르게 변경되기에 Site에서 정보를 확인 하는 것이 좋습니다.

#### For Windows
https://docs.docker.com/docker-for-windows/

#### For Mac
https://docs.docker.com/docker-for-mac/install/

#### For Linux
https://docs.docker.com/install/linux/docker-ce/ubuntu/


### Docker Hub 가입

Docker image 들을 검색 하고, Pull 하고 , Push 하기 위해서 계정 생성이 필요.

https://hub.docker.com/


#### 검색

~~~bash
docker search tomcat
~~~

#### Result
~~~bash
~/docker_temp  docker search tomcat                                                                                                                       12:51  10.28.2019
NAME                          DESCRIPTION                                     STARS               OFFICIAL            AUTOMATED
tomcat                        Apache Tomcat is an open source implementati…   2548                [OK]
tomee                         Apache TomEE is an all-Apache Java EE certif…   69                  [OK]
dordoka/tomcat                Ubuntu 14.04, Oracle JDK 8 and Tomcat 8 base…   53                                      [OK]
bitnami/tomcat                Bitnami Tomcat Docker Image                     29                                      [OK]
kubeguide/tomcat-app          Tomcat image for Chapter 1                      28
consol/tomcat-7.0             Tomcat 7.0.57, 8080, "admin/admin"              16                                      [OK]
cloudesire/tomcat             Tomcat server, 6/7/8                            15                                      [OK]
aallam/tomcat-mysql           Debian, Oracle JDK, Tomcat & MySQL              11                                      [OK]
arm32v7/tomcat                Apache Tomcat is an open source implementati…   9
rightctrl/tomcat              CentOS , Oracle Java, tomcat application ssl…   5                                       [OK]
unidata/tomcat-docker         Security-hardened Tomcat Docker container.      4                                       [OK]
maluuba/tomcat7-java8         Tomcat7 with java8.                             4
arm64v8/tomcat                Apache Tomcat is an open source implementati…   2
amd64/tomcat                  Apache Tomcat is an open source implementati…   2
ppc64le/tomcat                Apache Tomcat is an open source implementati…   1
99taxis/tomcat7               Tomcat7                                         1                                       [OK]
i386/tomcat                   Apache Tomcat is an open source implementati…   1
camptocamp/tomcat-logback     Docker image for tomcat with logback integra…   1                                       [OK]
oobsri/tomcat8                Testing CI Jobs with different names.           1
secoresearch/tomcat-varnish   Tomcat and Varnish 5.0                          0                                       [OK]
picoded/tomcat7               tomcat7 with jre8 and MANAGER_USER / MANAGER…   0                                       [OK]
appsvc/tomcat                                                                 0
s390x/tomcat                  Apache Tomcat is an open source implementati…   0
jelastic/tomcat               An image of the Tomcat Java application serv…   0
cfje/tomcat-resource          Tomcat Concourse Resource                       0
~~~

https://hub.docker.com/search?q=tomcat&type=image 에 접속 하여 Resource를 검색한 결과와 동일하며...
docker hub 사이트에서는 좀 더 detail 한 정보들이 확인 가능하다.

Ex) Docker File link
https://github.com/docker-library/tomcat/blob/8348a86101660e76224afcc57c4c9fae4dc76de8/9.0/jdk13/openjdk-oracle/Dockerfile


~~~bash
 ~/docker_temp  docker run tomcat                                                                                                                          00:15  10.29.2019
Unable to find image 'tomcat:latest' locally
latest: Pulling from library/tomcat
9a0b0ce99936: Pull complete
db3b6004c61a: Pull complete
f8f075920295: Pull complete
6ef14aff1139: Pull complete
962785d3b7f9: Pull complete
631589572f9b: Pull complete
c55a0c6f4c7b: Pull complete
379605d88e88: Pull complete
e056aa10ded8: Pull complete
6349a1c98d85: Pull complete
Digest: sha256:77e41dbdf7854f03b9a933510e8852c99d836d42ae85cba4b3bc04e8710dc0f7
Status: Downloaded newer image for tomcat:latest
28-Oct-2019 15:16:22.584 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Server version name:   Apache Tomcat/8.5.47
28-Oct-2019 15:16:22.590 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Server built:          Oct 7 2019 13:30:46 UTC
28-Oct-2019 15:16:22.591 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Server version number: 8.5.47.0
28-Oct-2019 15:16:22.592 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log OS Name:               Linux
28-Oct-2019 15:16:22.593 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log OS Version:            4.9.184-linuxkit
28-Oct-2019 15:16:22.594 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Architecture:          amd64
28-Oct-2019 15:16:22.595 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Java Home:             /usr/local/openjdk-8/jre
28-Oct-2019 15:16:22.596 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log JVM Version:           1.8.0_232-b09
28-Oct-2019 15:16:22.596 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log JVM Vendor:            Oracle Corporation
28-Oct-2019 15:16:22.597 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log CATALINA_BASE:         /usr/local/tomcat
28-Oct-2019 15:16:22.598 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log CATALINA_HOME:         /usr/local/tomcat
28-Oct-2019 15:16:22.598 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Djava.util.logging.config.file=/usr/local/tomcat/conf/logging.properties
28-Oct-2019 15:16:22.599 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager
28-Oct-2019 15:16:22.600 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Djdk.tls.ephemeralDHKeySize=2048
28-Oct-2019 15:16:22.600 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Djava.protocol.handler.pkgs=org.apache.catalina.webresources
28-Oct-2019 15:16:22.601 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Dorg.apache.catalina.security.SecurityListener.UMASK=0027
28-Oct-2019 15:16:22.601 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Dignore.endorsed.dirs=
28-Oct-2019 15:16:22.602 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Dcatalina.base=/usr/local/tomcat
28-Oct-2019 15:16:22.602 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Dcatalina.home=/usr/local/tomcat
28-Oct-2019 15:16:22.603 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Djava.io.tmpdir=/usr/local/tomcat/temp
28-Oct-2019 15:16:22.603 INFO [main] org.apache.catalina.core.AprLifecycleListener.lifecycleEvent Loaded APR based Apache Tomcat Native library [1.2.23] using APR version [1.5.2].
28-Oct-2019 15:16:22.604 INFO [main] org.apache.catalina.core.AprLifecycleListener.lifecycleEvent APR capabilities: IPv6 [true], sendfile [true], accept filters [false], random [true].
28-Oct-2019 15:16:22.604 INFO [main] org.apache.catalina.core.AprLifecycleListener.lifecycleEvent APR/OpenSSL configuration: useAprConnector [false], useOpenSSL [true]
28-Oct-2019 15:16:22.612 INFO [main] org.apache.catalina.core.AprLifecycleListener.initializeSSL OpenSSL successfully initialized [OpenSSL 1.1.0l  10 Sep 2019]
28-Oct-2019 15:16:22.740 INFO [main] org.apache.coyote.AbstractProtocol.init Initializing ProtocolHandler ["http-nio-8080"]
28-Oct-2019 15:16:22.763 INFO [main] org.apache.tomcat.util.net.NioSelectorPool.getSharedSelector Using a shared selector for servlet write/read
28-Oct-2019 15:16:22.779 INFO [main] org.apache.coyote.AbstractProtocol.init Initializing ProtocolHandler ["ajp-nio-8009"]
28-Oct-2019 15:16:22.781 INFO [main] org.apache.tomcat.util.net.NioSelectorPool.getSharedSelector Using a shared selector for servlet write/read
28-Oct-2019 15:16:22.782 INFO [main] org.apache.catalina.startup.Catalina.load Initialization processed in 785 ms
28-Oct-2019 15:16:22.832 INFO [main] org.apache.catalina.core.StandardService.startInternal Starting service [Catalina]
28-Oct-2019 15:16:22.833 INFO [main] org.apache.catalina.core.StandardEngine.startInternal Starting Servlet Engine: Apache Tomcat/8.5.47
28-Oct-2019 15:16:22.851 INFO [localhost-startStop-1] org.apache.catalina.startup.HostConfig.deployDirectory Deploying web application directory [/usr/local/tomcat/webapps/manager]
28-Oct-2019 15:16:23.289 INFO [localhost-startStop-1] org.apache.catalina.startup.HostConfig.deployDirectory Deployment of web application directory [/usr/local/tomcat/webapps/manager] has finished in [437] ms
28-Oct-2019 15:16:23.290 INFO [localhost-startStop-1] org.apache.catalina.startup.HostConfig.deployDirectory Deploying web application directory [/usr/local/tomcat/webapps/examples]
28-Oct-2019 15:16:23.530 INFO [localhost-startStop-1] org.apache.catalina.startup.HostConfig.deployDirectory Deployment of web application directory [/usr/local/tomcat/webapps/examples] has finished in [240] ms
28-Oct-2019 15:16:23.531 INFO [localhost-startStop-1] org.apache.catalina.startup.HostConfig.deployDirectory Deploying web application directory [/usr/local/tomcat/webapps/ROOT]
28-Oct-2019 15:16:23.545 INFO [localhost-startStop-1] org.apache.catalina.startup.HostConfig.deployDirectory Deployment of web application directory [/usr/local/tomcat/webapps/ROOT] has finished in [14] ms
28-Oct-2019 15:16:23.546 INFO [localhost-startStop-1] org.apache.catalina.startup.HostConfig.deployDirectory Deploying web application directory [/usr/local/tomcat/webapps/docs]
28-Oct-2019 15:16:23.577 INFO [localhost-startStop-1] org.apache.catalina.startup.HostConfig.deployDirectory Deployment of web application directory [/usr/local/tomcat/webapps/docs] has finished in [31] ms
28-Oct-2019 15:16:23.577 INFO [localhost-startStop-1] org.apache.catalina.startup.HostConfig.deployDirectory Deploying web application directory [/usr/local/tomcat/webapps/host-manager]
28-Oct-2019 15:16:23.601 INFO [localhost-startStop-1] org.apache.catalina.startup.HostConfig.deployDirectory Deployment of web application directory [/usr/local/tomcat/webapps/host-manager] has finished in [24] ms
28-Oct-2019 15:16:23.609 INFO [main] org.apache.coyote.AbstractProtocol.start Starting ProtocolHandler ["http-nio-8080"]
28-Oct-2019 15:16:23.630 INFO [main] org.apache.coyote.AbstractProtocol.start Starting ProtocolHandler ["ajp-nio-8009"]
28-Oct-2019 15:16:23.636 INFO [main] org.apache.catalina.startup.Catalina.start Server startup in 853 ms

~~~

http://localhost:8080 으로 접속 -> 접속 안 됨


### Tomcat 을 포트 포워딩으로 실행하기

~~~bach
docker run -d -t -p 8080:8080 tomcat
~~~

- d Detached Mode ( Demon Mode)
- t tty (Tele Type 
- ports local port:docker post


* Browser 로 http:\\localhost:8080 으로 연결 가능



#### image 내려받기
~~~bash
docker pull tomcat
~~~

#### 내려받은 image 조회
~~~bash
docker images

 ~/docker_temp  docker images                                                                                                                              00:30  10.29.2019
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
tomcat              latest              882487b8be1d        9 days ago          507MB
onetest             1.3                 b5c1d83654df        2 weeks ago         121MB
onetest             1.2                 076cebe4faec        2 weeks ago         121MB
ubuntu              16.04               657d80a6401d        5 weeks ago         121MB
onetest             1.1                 657d80a6401d        5 weeks ago         121MB
ubuntu              latest              2ca708c1c9cc        5 weeks ago         64.2MB
mysql               latest              b8fd9553f1f0        6 weeks ago         445MB
~~~

#### image 삭제하기
~~~bash
docker rmi {id or name}
~~~



#### Container 확인하기

~~~bash
#실행중인 container 
docker ps

#모든 container
docker ps -a


#위와 동일
docker container ls
docker container ls -a

~~~

#### Container 정지

~~~bash
docker stop {id or name}
~~~


#### Container 시작
~~~bash
docker start {id or name}
~~~

#### Container 삭제
~~~bash
docker rm {id or name}
~~~


### Docker Build / Mount

https://docs.docker.com/engine/reference/builder/

#### Dockerfile

~~~bash

~~~


#### Volume

~~~bash

~~~
참고: 
https://joont92.github.io/docker/volume-container-%EC%B6%94%EA%B0%80%ED%95%98%EA%B8%B0/



#### docker-compose

참고: Dockerfile 로 기본 지정하고 docker-compose 로 port 설정 
https://kycfeel.github.io/2017/03/15/DockerFile%EA%B3%BC-Docker-Compose/


~~~bash

version: '2'
services:
        db:
                image: mysql:latest
                environment:
                        MYSQL_ROOT_PASSWORD: 1234
                        MYSQL_DATABASE: test
                        MYSQL_USER: user
                        MYSQL_PASSWORD: 1234
                ports:
                        - "3306:3306"
                volumes:
                        - "./db:/docker-entrypoint-initdb.d"
                restart: always

        web:
                image: tomcat:latest
                environment:
                        JDBC_URL: jdbc:mysql://db:3306/test?connectTimeout=0&amp;socketTimeout=0&amp;autoReconnect=true
                        JDBC_USER: user
                        JDBC_PASS: 1234
                ports:
                        - "80:8080"
                volumes:
                        - ./tomcat/webapps:/user/local/tomcat/webapps
                links:
                        - db
                restart: always
				
~~~




Kubernetes

- 쿠버네티스란??
- GCP에서 k8s 맛보기
- 











